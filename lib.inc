%define WRITE_SYSCALL 1
%define EXIT_SYSCALL 60
%define READ_FD 0
%define WRITE_FD 1
%define NEW_LINE_CHAR 10
%define TAB 0x9
%define SPACE 0x20
%define MINUS '-'
%define MIN '0'
%define MAX '9'


section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    pop rsi
    mov rdi, WRITE_FD
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE_CHAR


; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov [rsp], dil
    mov rdi, WRITE_FD
    mov rsi, rsp
    mov rax, WRITE_SYSCALL
    mov rdx, 1 ; количество выводимых символов
    inc rsp
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jge .positive
    .negative:
        push rdi
        mov rdi, MINUS
        call print_char
        pop rdi
        neg rdi
    .positive:

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10 ; делитель
    mov rcx, rsp
    sub rsp, 32 ; отступ для сохранения строки на стеке
    dec rcx
    mov byte[rcx], 0
    mov rax, rdi
    .loop:
    	xor rdx, rdx
    	div r8
    	add rdx, MIN
    	dec rcx
    	mov byte[rcx], dl
        test rax, rax
    	jne .loop
    mov rdi, rcx
    call print_string
    add rsp, 32
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax  ; Обнуляем rax (будем использовать для результата)
    xor rcx, rcx  ; Счетчик для индекса символов

    push rbx
    .compare_loop:
        movzx rdx, byte [rdi + rcx]  ; Загружаем байт из первой строки
        movzx rbx, byte [rsi + rcx]  ; Загружаем байт из второй строки
        cmp rdx, rbx                  ; Сравниваем байты
        jne .not_equal                ; Если не равны, переходим к .not_equal
        test rdx, rdx                    ; Проверяем, достигнут ли конец строки
        je .strings_equal             ; Если конец строки, строки равны

        inc rcx                       ; Инкрементируем счетчик
        jmp .compare_loop             ; Переходим к следующему символу

    .strings_equal:
        mov rax, 1  ; Строки равны, сохраняем 1 в rax
        pop rbx
        ret

    .not_equal:
        xor rax, rax  ; Строки не равны, сохраняем 0 в rax
        pop rbx
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax            ; Обнуляем rax (будем использовать для результата)
    mov rdi, READ_FD        ; stdin (файловый дескриптор)
    lea rsi, [rsp - 1]      ; Указатель на буфер размером 1 байт
    mov rdx, 1              ; Читаем один байт
    syscall                 ; Системный вызов read

    test rax, rax              ; Проверяем результат read
    jle .end_of_stream      ; Если результат <= 0, значит достигнут конец потока

    movzx rax, byte[rsp - 1]  ; Загружаем прочитанный байт в rax
    ret

.end_of_stream:
    xor rax, rax            ; Возвращаем 0 в rax, если достигнут конец потока
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax
    mov r10, rdi
    push rsi
    push r10
    push rdi
    .next_iteration:
        call read_char
        cmp rax, SPACE
        je .next_iteration
        cmp rax, NEW_LINE_CHAR
        je .next_iteration
        cmp rax, TAB
        je .next_iteration
    pop rdi
    pop r10
    pop rsi
    .read:
        test rax, rax
        je .empy_string
        cmp rax, SPACE
        jz .empy_string
        cmp rax, NEW_LINE_CHAR
        jz .empy_string
        cmp rax, TAB
        jz .empy_string

        dec rsi
        test rsi, rsi
        jz .err
        mov byte[rdi], al
        inc rdi

        push rdi
        push rsi
        push r10
        call read_char
        pop r10
        pop rsi
        pop rdi
        jmp .read
    .empy_string:
        mov byte[rdi], 0
        mov rax, r10
        sub rdi, r10
        mov rdx, rdi
        ret
    .err:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r10, 10
    xor r9, r9 ; len
.parse_uint_loop:
    mov cl, [rdi + r9]
    cmp cl, MAX
    ja .parse_uint_loop_end
    cmp cl, MIN
    jb .parse_uint_loop_end

    sub cl, MIN
    mul r10
    add rax, rcx
    add r9, 1
    jmp .parse_uint_loop

.parse_uint_loop_end:
    mov rdx, r9
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r10, r10 ; sign
    xor r9, r9 ; len
    cmp byte [rdi], MINUS
    jne .parse_int_after_sign
    add r9, 1
    add r10, 1
    add rdi, 1
.parse_int_after_sign:
    push r10
    push r9
    call parse_uint
    pop r9
    pop r10
    test rdx, rdx
    jne .parse_int_no_err
    ret
.parse_int_no_err:
    cmp r10, 1
    jne .parse_int_after_inversion
    neg rax
.parse_int_after_inversion:
    add rdx, r9
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.strcpy_loop:
    mov cl, [rdi + rax]
    mov [rsi + rax], cl
    add rax, 1
    cmp rax, rdx
    jna .strcpy_loop_no_err
    xor rax, rax
    ret
.strcpy_loop_no_err:
    cmp byte [rdi + rax], 0
    jne .strcpy_loop
    mov byte [rsi + rax], 0
    ret